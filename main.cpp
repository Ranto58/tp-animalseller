#include <iostream>
#include "zoo.h"
#include "animal.h"
#include "animalvendor.h"
#include "animalexception.h"
using namespace std;

int main() {
    AnimalVendor *vendor = new AnimalVendor();
    vendor->Register("Poule", &Poule::create);
    vendor->Register("Aigle", &Aigle::create);
    vendor->Register("Tigre", &Aigle::create);

    //Creation des animaux create(nombre, type, prix_de_vente);
    vendor->create(10, "Poule", 20);
    vendor->create(4, "Aigle",150);
    vendor->create(5, "Tigre", 1000);

    // affichage des infos du vendeur dont son stock
    qDebug().noquote() << *vendor;

    Zoo* zoo = new Zoo();
    zoo->setBudgetAmount(3000);
    qDebug() << zoo->getBudgetAmount();

    // Achat des animaux : buy(vendeur, nb_animaux, type)
    zoo->buy(vendor, 5, "Poule"); // 100
    zoo->buy(vendor, 4, "Aigle"); // 600
    zoo->buy(vendor, 2, "Tigre"); // 2000 -> total 2700 EUR

    zoo->buy(vendor, 5, "Poule"); // 100
    zoo->buy(vendor, 4, "Aigle"); // plus assez d'aigle
    zoo->buy(vendor, 2, "Tigre"); // plus assez d'argent

    // le vendeur et ses animaux sont detruits
    delete vendor;

    // affichage des infos du zoo dont son inventaire d'animaux et l'=état de son budget
    qDebug().noquote() << *zoo;
}

