#ifndef ANIMALEXCEPTION_H
#define ANIMALEXCEPTION_H
#include <QString>

class AnimalException
{
    QString m_msg;
public:
    AnimalException(QString msg);
    QString msg() const;
    void setMsg(const QString &msg);
};

#endif // ANIMALEXCEPTION_H
