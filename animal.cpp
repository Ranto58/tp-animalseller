#include "animal.h"

void Animal::setName(const QString &name)
{
    _name = name;
}

void Animal::setType(const QString &type)
{
    _type = type;
}

void Animal::setPrice(int price)
{
    _price = price;
}

Animal::Animal()
{
    
}

Animal::~Animal()
{
    qDebug() << __FUNCTION__ << type() << name();
}

QString Animal::name()
{
    return _name;
}

QString Animal::type() const
{
    return _type;
}

int Animal::price()
{
   return _price;
}
