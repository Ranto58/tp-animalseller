#ifndef ANIMAL_H
#define ANIMAL_H
#include <QString>
#include <QDebug>

class Animal
{
    QString _name;
    QString _type;
    int _price;
public:
    Animal();
    virtual ~Animal();
    virtual QString name();
    virtual QString type() const;
    virtual int price();
    void setName(const QString &name);
    void setType(const QString &type);
    void setPrice(int price);
};

template <class T>
class TAnimal : public Animal{
public:
    static Animal *create(){
        qDebug() << __FUNCTION__;
        return new T;
    }
};

#define CREATE_ANIMAL_CLASS(animal)                                                                                    \
    class animal : public TAnimal<animal> {}


CREATE_ANIMAL_CLASS(Poule);
CREATE_ANIMAL_CLASS(Tigre);
CREATE_ANIMAL_CLASS(Aigle);

#endif // ANIMAL_H
