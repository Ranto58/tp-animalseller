#include "animalvendor.h"

AnimalVendor::AnimalVendor()
{
    qDebug() << __FUNCTION__;
    Register("Poule", &Poule::create);
    Register("Tigre", &Tigre::create);
}

AnimalVendor::~AnimalVendor()
{
    foreach (auto animal, animalList["Poule"])
    {
        delete animal;
    }
    foreach (auto animal, animalList["Tigre"])
    {
        delete animal;
    }
    foreach (auto animal, animalList["Aigle"])
    {
        delete animal;
    }
}

void AnimalVendor::Register(QString animalName, CreateAnimalFn fn)
{
    qDebug() << __FUNCTION__ << animalName;
    m_factoryMap[animalName] = fn;
}

bool AnimalVendor::buyAnimal(int number, QString animalName, Zoo *zoo)
{
    //Verifier si yen a assez
    if(animalList[animalName].size() < number) {
        return false;
    }
    // Trouver le prix
    int price = animalList[animalName].at(0)->price() * number;

    // Verifier quon a assez
    if(zoo->getBudgetAmount() < price) {
        qDebug() << "not enough money";
        return false;
    }

    zoo->setBudgetAmount(zoo->getBudgetAmount() - price);
    //Ajouter les animaux a la liste du zoo
    for(int i = 0; i < number; i++) {
        zoo->addAnimal(animalName, animalList[animalName].at(i));
    }
    return true;
}

void AnimalVendor::create(int number, QString animalName, int price)
{
    qDebug() << __FUNCTION__ << animalName;
    qDebug() << __FUNCTION__ << number;
    qDebug() << __FUNCTION__ << price;

    CreateAnimalFn fn = m_factoryMap[animalName];
    if(fn == nullptr){
        throw AnimalException("Unable to create" + animalName);
    }

    //number
    for(int i =0; i < number; i++){
        Animal *animal = fn();
        animal->setType(animalName);
        animal->setName(QString(animalName + " #%1").arg(animalList.size()));
        animal->setPrice(price);
        animalList[animalName].append(animal);
    }
}





AnimalVendor::operator QString() const
{
    QString ret;
    ret += "ACTIONS MAP:\n";
    foreach (auto key, m_factoryMap.keys()) {
        ret += " > " + key + ": " + STR_PTR(m_factoryMap[key]);
        ret += '\n';
    }

    ret += "ANIMAL LIST:\n";

    foreach (Animal *animal, animalList["Poule"]) {
        ret += " > " + animal->type() + ": " + animal->name();
        ret += '\n';
    }

    return ret;
};


