#ifndef ZOO_H
#define ZOO_H
#include <QString>
#include "animalvendor.h"
#include "animal.h"
class AnimalVendor;
class Zoo
{
    int BudgetAmount;
    QMap<QString, QList<Animal *>> animalList;

public:
    Zoo();
//    bool buy(AnimalVendor *vendor, int nb_animaux, QString animalName);
    int getBudgetAmount() const;
    void setBudgetAmount(int value);
    int getAnimalCount();
    void addAnimal(QString animalName, Animal* animal);
    bool buy(AnimalVendor *vendor, int nb_animaux, QString animalName);
    operator QString() const;
};

#endif // ZOO_H
