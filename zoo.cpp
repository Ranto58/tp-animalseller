#include "zoo.h"

void Zoo::setBudgetAmount(int value)
{
    BudgetAmount = value;
}

void Zoo::addAnimal(QString animalName, Animal *animal)
{
    animalList[animalName].append(animal);
}


int Zoo::getBudgetAmount() const
{
    return BudgetAmount;
}

Zoo::Zoo()
{
    
}

bool Zoo::buy(AnimalVendor *vendor, int nb_animaux, QString animalName)
{
    return vendor->buyAnimal(nb_animaux, animalName, this);
}

Zoo::operator QString() const
{
    QString ret;

    ret += "ZOO:\n";
    ret += "Budget" + QString::number(BudgetAmount);

    return ret;
}

