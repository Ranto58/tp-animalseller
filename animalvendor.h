﻿#ifndef ANIMALVENDOR_H
#define ANIMALVENDOR_H
#include "zoo.h"
#include "animal.h"
#include "animalexception.h"
#include <QMap>
#include <QString>


#define STR_PTR(ptr) QString("0x%1").arg(reinterpret_cast<quintptr>(ptr), QT_POINTER_SIZE * 2, 16, QChar('0'))


typedef Animal *(*CreateAnimalFn)();
class Zoo;
class AnimalVendor
{
    QMap<QString, CreateAnimalFn> m_factoryMap;
//    QList<Animal *> animalList;
    QMap<QString, QList<Animal *>> animalList;

public:
    AnimalVendor();
    ~AnimalVendor();
    void Register(QString animalName, CreateAnimalFn fn);
    bool buyAnimal(int number, QString animalName, Zoo *zoo);
    operator QString() const;
    void create(int number, QString animalName, int price);

};


#endif // ANIMALVENDOR_H
