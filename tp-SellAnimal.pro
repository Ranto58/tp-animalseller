TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += qt

SOURCES += \
        animal.cpp \
        animalexception.cpp \
        animalvendor.cpp \
        main.cpp \
        zoo.cpp

HEADERS += \
    animal.h \
    animalexception.h \
    animalvendor.h \
    zoo.h
